import styled from "@emotion/styled";
import dayjs from "dayjs";

import NavArrow from "../../../../ui/atoms/NavArrow";
import typography from "../../../../../../utils/typography";

export default function CalendarHeader({ currentDate, nextMonth, prevMonth }) {
  return (
    <CalendarNavigation>
      <div className="calendar-nav-arrow" onClick={prevMonth}>
        <NavArrow type={"prev"} />
      </div>
      <div className="calendar-header-label">
        {dayjs(currentDate).format("MMMM")}
      </div>
      <div className="calendar-nav-arrow" onClick={nextMonth}>
        <NavArrow type={"next"} />
      </div>
    </CalendarNavigation>
  );
}

const CalendarNavigation = styled.header`
  background: #ddf2ff;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 14px 30px 30px;

  @media screen and (max-width: 602px) {
    padding: 16px;
  }

  .calendar-nav-arrow {
    border: none;
    background: transparent;
    box-sizing: border-box;
    overflow: hidden;
    outline: none;
    cursor: pointer;
  }

  .calendar-header-label {
    background: transparent;
    border: none;
    color: ##242220;
    ${typography.body1_d};
    font-weight: 600;
    flex-grow: 0 !important;
    text-align: ceimport NavArrow from './../../../../ui/atoms/NavArrow/index';
nter;

    @media screen and (max-width: 602px) {
      ${typography.body1_m};
    }
  }
`;
