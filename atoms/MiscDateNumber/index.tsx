import { css } from "@emotion/core";
import styled from "@emotion/styled";
import typography from "../../../../../../utils/typography";
import dayjs from "dayjs";

export default function MiscDateNumber({
  date,
  disable,
  today,
  selected,
  params = []
}) {
  return (
    <DateNumber
      disable={disable}
      className="day"
      today={today}
      selected={selected}
      data-date={dayjs(date).toString()}
      data-params={params.map(el=>el._id)}
      params={params}
    >
      <div className="day-number">{dayjs(date).format("D")}</div>
      <>{renderParam(params)}</>
    </DateNumber>
  );
}

function renderParam(params) {
  return (
    <>
      {params.length === 0 && <></>}
      {params.length === 1 && (
        <StatusELem status={params[0].statusData} className="day-param" />
      )}
      {params.length > 1 && (
        <StatusELem status={"multi"} className="day-param" />
      )}
    </>
  );
}

const dynamicStyles = props => css`
  border-radius: 4px;
  background: ${props.today
    ? "#1075b9"
    : !props.selected
    ? "#ffffff"
    : "#ddf2ff"};
  color: ${props.disable ? "#9aaebb" : !props.today ? "#242220" : "#f9f9f9"};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 48px;
  height: ${props.params.length === 0 ? "48px" : "56px"};
  ${typography.body1_m};
  pointer-events: ${props.disable ? "none" : "all"};

  .day-param{
    pointer-events: none;
  }

  .day-number {
    pointer-events: none;
  }

  &:hover {
    background: ${!props.today ? "#ddf2ff" : ""};
    cursor: pointer;
  }

  @media screen and (max-width: 620px) {
    width: 32px;
    height: ${props.params.length === 0 ? "32px" : "37px"};
    ${typography.body3_m};
  }
`;

const DateNumber = styled.div`
  ${dynamicStyles}
`;

const dynamicStatusStyles = props => css`
  width: 11px;
  height: 11px;
  border-radius: 50%;
  background: ${props.status === "pending"
    ? "#ffc800"
    : props.status === "accepted"
    ? "#00a735"
    : props.status === "declined"
    ? "#d30033"
    : "#5b7281"};
`;

const StatusELem = styled.div`
  ${dynamicStatusStyles};
`;
