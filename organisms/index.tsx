import dayjs from "dayjs";
import styled from "@emotion/styled";
import React, { useState } from "react";

import Weekdays from "../molecules/Weekdays";
import CalendarHeader from "../molecules/CalendarHeader";
import Days from "../molecules/Days";

export default function Calendar(props) {
  const [currentDate, setCurrentDate] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState(null);
  const [todayDate, setTodayDate] = useState(new Date());
  const [rowUnderline, setRowUnderlineOpen] = useState(-1);
  const [viewingsByDate, setViewingsByDate] = useState([]);
  const [viewingsOpenIndex, setViewingsOpenIndex] = useState(-1);

  const toggle = index => {
    return index === viewingsOpenIndex
      ? setViewingsOpenIndex(-1)
      : setViewingsOpenIndex(index);
  };

  const nextMonth = () => {
    let nextMonthDate = new Date(
      dayjs(currentDate)
        .add(1, "month")
        .toString()
    );
    setRowUnderlineOpen(-1);
    setCurrentDate(nextMonthDate);
  };

  const prevMonth = () => {
    let prevMonthDate = new Date(
      dayjs(currentDate)
        .subtract(1, "month")
        .toString()
    );
    setRowUnderlineOpen(-1);
    setCurrentDate(prevMonthDate);
  };

  const handleClickedDay = (e, j) => {
    const targetDataset = e.target.dataset;
    if (targetDataset.params.length === 0) {
      setSelectedDate(new Date(targetDataset.date));
      setRowUnderlineOpen(-1);
    } else {
      setSelectedDate(new Date(targetDataset.date));
      setViewingsByDate(
        props.viewings.filter(el => targetDataset.params.includes(el._id))
      );
      j === rowUnderline &&
      dayjs(targetDataset.date).isSame(dayjs(selectedDate))
        ? setRowUnderlineOpen(-1)
        : setRowUnderlineOpen(j);
    }
  };

  return (
    <CalendarWrap className="calendar">
      <CalendarHeader
        currentDate={currentDate}
        nextMonth={nextMonth}
        prevMonth={prevMonth}
      />
      <CalendarViewContainer>
        <Weekdays />
        <Days
          currentDate={currentDate}
          selectedDate={selectedDate}
          todayDate={todayDate}
          handleClickedDay={handleClickedDay}
          viewings={props.viewings}
          rowUnderLine={rowUnderline}
          viewingsByDate={viewingsByDate}
          viewingsOpenIndex={viewingsOpenIndex}
          toggle={toggle}
        />
      </CalendarViewContainer>
    </CalendarWrap>
  );
}

const CalendarWrap = styled.div``;

const CalendarViewContainer = styled.div`
  border-radius: 3px;
  width: 1037px;
  height: 100%;
  margin: 0 auto;
  background: #ffffff;
  box-sizing: border-box;

  @media screen and (max-width: 602px) {
    width: 333px;
  }
`;
